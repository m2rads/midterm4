const addPosts = posts => {
  $('#signups > tbody:last-child').append(
    `<tr>
      <td>${posts.author}</td>
      <td>${posts.title}</td>
      <td>${posts.body}</td>
    </tr>`
  );
};

const showPosts = async postService => {
  // Find the latest posts. They will come with the newest first
  const posts = await postService.find({
    query: {
      $sort: { createdAt: -1 },
      $limit: 25
    }
  });

  // We want to show the newest signup last
  signups.data.reverse().forEach(addSignup);
};


// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  'use strict';

  window.addEventListener('load', async function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `posts` service
    const posts = app.service('posts');

    // ADD REQUIRED CODE HERE
    showPosts(posts);



  }, false);
}());
